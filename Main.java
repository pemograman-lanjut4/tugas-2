package PemlanLaura;

import java.util.ArrayList;
import java.util.Scanner;

import PemlanKayla.Mahasiswa;

public class MainClass {
    @SuppressWarnings("resource")
    public static void main(String[] args) {
        ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        boolean tambahLagi = true;
        while (tambahLagi) {
            System.out.print("Masukkan nim: ");
            String nim = scanner.nextLine();

            System.out.print("Masukkan nama: ");
            String nama = scanner.nextLine();

            System.out.print("Masukkan alamat: ");
            String alamat = scanner.nextLine();

            Mahasiswa mahasiswa = new Mahasiswa(nim, nama, alamat);
            daftarMahasiswa.add(mahasiswa);

            System.out.print("Tambah lagi? (y/n): ");
            String tambah = scanner.nextLine();
            tambahLagi = tambah.equalsIgnoreCase("y");
        }

        System.out.println("==================================");
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            System.out.println(mahasiswa.getNim() + " | " + mahasiswa.getNama() + " | " + mahasiswa.getAlamat());
        }
    }
}
